@extends('layout/master')

@section('titleweb') Cast @endsection
@section('title') Detail Cast @endsection

@section('subtitle')
Data Cast
@endsection

@section('content')
<!-- <h3>Tambah Data Cast</h3> -->
<!-- <form action="/cast" method="POST">
    @csrf -->
<div class="form-group">
    <label for="nama">Nama</label>
    <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" disabled>
</div>
<div class="form-group">
    <label for="umur">Umur</label>
    <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" disabled>
</div>
<div class="form-group">
    <label for="bio">Bio</label>
    <textarea class="form-control @error('bio') is-invalid @enderror" name="bio" disabled>{{$cast->bio}}</textarea>
</div>

<a href="/cast" class="btn btn-primary btn-sm my-3">Kembali</a>
<!-- </form> -->
@endsection