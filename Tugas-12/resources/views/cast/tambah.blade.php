@extends('layout/master')

@section('titleweb') Cast @endsection
@section('title') Add Cast @endsection

@section('subtitle')
Data Cast
@endsection

@section('content')
<!-- <h3>Tambah Data Cast</h3> -->
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" placeholder="Nama Cast">
    </div>
    <div class=" form-group">
        <label for="umur">Umur</label>
        <input type="number" class="form-control @error('umur') is-invalid @enderror" name="umur" placeholder="Umur Cast">
    </div>
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control @error('bio') is-invalid @enderror" name="bio" placeholder="Bio Cast"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection