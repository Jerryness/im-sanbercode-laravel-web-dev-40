@extends('layout/master')

@section('titleweb') Cast Table @endsection

@section('title') Halaman Data Cast @endsection

@section('subtitle')
Data Cast
@endsection

@push('scripts')
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
    $(function() {
        $("#example1").DataTable();
    });
</script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.13.1/datatables.min.css" />
@endpush

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm mb-3">Tambah</a>
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <td>
                {{$key+1}}
            </td>
            <td>
                {{$item->nama}}
            </td>
            <td>
                {{$item->umur}}
            </td>
            <td>
                <form action="/cast/{{$item->id}}" method="post">
                    @method('delete')
                    @csrf
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">

                </form>
            </td>
        </tr>

        @empty
        <tr>
            <td colspan="4">
                Tidak ada data
            </td>
        </tr>
        @endforelse
    </tbody>
    <!-- <tfoot>
        <tr>
            <th>Rendering engine</th>
            <th>Browser</th>
            <th>Platform(s)</th>
            <th>Engine version</th>
            <th>CSS grade</th>
        </tr>
    </tfoot> -->
</table>

@endsection