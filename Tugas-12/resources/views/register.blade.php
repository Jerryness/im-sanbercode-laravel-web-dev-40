@extends('layout/master')

@section('titleweb') Register @endsection
@section('title') Halaman Register @endsection

@section('subtitle')
Buat Account Baru
@endsection

@section('content')
<h3>Sign Up Form</h3>
<form action="/welcome" method="POST">
    @csrf
    <label>First Name:</label><br>
    <input type="text" name="namadepan" required><br><br>
    <label>Last Name:</label><br>
    <input type="text" name="namabelakang" required><br><br>
    <label>Gender:</label><br>
    <input type="radio" name="gender" value="male">Male<br>
    <input type="radio" name="gender" value="female">Female<br>
    <input type="radio" name="gender" value="other">Other<br><br>
    <label>Nationality:</label><br>
    <select name="nationality">
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Wakanda">Wakanda</option>
        <option value="Konoha">Konoha</option>
    </select><br><br>
    <label>Language Spoken:</label><br>
    <input type="checkbox" name="Bahasa Indonesia">Bahasa Indonesia<br>
    <input type="checkbox" name="English">English<br>
    <input type="checkbox" name="Arabic">Arabic<br>
    <input type="checkbox" name="Javanese">Javanese<br><br>
    <label>Bio:</label><br>
    <textarea name="bio"></textarea><br><br>
    <input type="submit" value="Sign Up">
</form>
@endsection