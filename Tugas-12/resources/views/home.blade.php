@extends('layout/master')

@section('titleweb') Home @endsection
@section('title') Halaman Home @endsection

@section('subtitle')
Halaman Utama
@endsection

@section('content')
<h1>SanberBook</h1>
<h2>Social Media Developer Santai Berkualitas</h2>

<p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>

<h3>Benefit Join di SanberBook</h3>
<ul>
    <li>Mendafpatkan motivasi dari sesama developer</li>
    <li>Sharing knowledge dari para mastah Sanber</li>
    <li>Dibuar oleh calon web developer terbarik</li>
</ul>
<h3>Cara Bergabung ke SanberBook</h3>
<ol>
    <li>Mengunjuki Website ini</li>
    <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
    <li>Selesai!</li>
</ol>
@endsection