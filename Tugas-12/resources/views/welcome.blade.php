@extends('layout/master')

@section('titleweb') Dashboard @endsection
@section('title') Halaman Dashboard @endsection

@section('subtitle')
Welcome
@endsection

@section('content')
<h1>Selamat Datang {{$namadepan}} {{$namabelakang}}!</h1>
<h2>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h2>
@endsection