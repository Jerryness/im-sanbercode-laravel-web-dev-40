<?php

use App\Http\Controllers\authController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\homeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Route::get('/', [homeController::class, 'home']);

Route::get('/register', [authController::class, 'register']);

Route::post('/welcome', [authController::class, 'kirim']);

// Route::get('/master', function () {
//     return view('layout/master');
// });

Route::get('/table', function () {
    return view('/table');
});

Route::get('/data-table', function () {
    return view('/data-table');
});

//tampil
Route::get('/cast', [CastController::class, 'index']);

//imput
Route::get('/cast/create', [CastController::class, 'create']);

//save input
Route::post('/cast', [CastController::class, 'store']);

//tampil detail
Route::get('/cast/{id}', [CastController::class, 'show']);

//edit
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);

//edit save
Route::put('/cast/{id}', [CastController::class, 'update']);

//edit save
Route::delete('/cast/{id}', [CastController::class, 'destroy']);
