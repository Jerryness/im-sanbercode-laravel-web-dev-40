<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $cast = DB::table('casts')->get();
        return view('cast/tampil', ['cast' => $cast]);
    }

    public function create()
    {
        return view('cast/tambah');
    }

    public function store(Request $request)
    {

        $validated = $request->validate(
            [
                //ini untuk validasi
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required',
            ],
            [
                //ini untuk custom aler validasi
                'nama.required' => 'Nama wajib diisi',
                'umur.required' => 'Umur wajib diisi',
                'bio.required' => 'Bio wajib diisi',
            ]

        );
        DB::table('casts')->insert([
            'nama' => $request['nama'],
            'umur' =>  $request['umur'],
            'bio' =>  $request['bio']
        ]);

        return redirect('cast');
    }

    public function show($id)
    {
        $cast = DB::table('casts')->find($id);
        return view('cast/detail', ['cast' => $cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('casts')->find($id);
        return view('cast/edit', ['cast' => $cast]);
    }

    public function update($id, Request $request)
    {
        $validated = $request->validate(
            [
                //ini untuk validasi
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required',
            ],
            [
                //ini untuk custom aler validasi
                'nama.required' => 'Nama wajib diisi',
                'umur.required' => 'Umur wajib diisi',
                'bio.required' => 'Bio wajib diisi',
            ]

        );

        DB::table('casts')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama'],
                    'umur' =>  $request['umur'],
                    'bio' =>  $request['bio']
                ]
            );

        return redirect('cast');
    }

    public function destroy($id)
    {
        DB::table('casts')->where('id', $id)->delete();

        return redirect('cast');
    }
}
