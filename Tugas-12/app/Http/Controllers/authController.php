<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use function PHPSTORM_META\registerArgumentsSet;

class authController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function kirim(Request $request)
    {
        // return view('welcome');
        $namadepan = $request['namadepan'];
        $namabelakang = $request['namabelakang'];
        return view('welcome', ['namadepan' => $namadepan, 'namabelakang' => $namabelakang]);
    }
}
